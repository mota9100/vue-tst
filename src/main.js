import Vue from 'vue'
import HelloWorld from './components/HelloWorld.vue'

Vue.component('hello-world', HelloWorld)

Vue.config.productionTip = false

document.getElementById('app').innerHTML = `<div>
    <img alt="Vue logo" src="./assets/logo.png">
    <hello-world msg="Welcome to Your Vue.js App"/>
  </div>`

new Vue().$mount('#app')
